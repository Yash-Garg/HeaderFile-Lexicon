# HeaderFile-Lexicon 

[![MIT License](https://img.shields.io/crates/l/rustc-serialize.svg)](https://github.com/Blank-One/HeaderFile-Lexicon/blob/master/LICENSE) [![GitHub code size in bytes](https://img.shields.io/github/languages/code-size/badges/shields.svg)](https://github.com/Blank-One/HeaderFile-Lexicon/blob/master/lexicon.h)

````
 _        ______   __   __  _____    _____    ____    _   _
| |      |  ____|  \ \ / / |_   _|  / ____|  / __ \  | \ | |
| |      | |__      \ V /    | |   | |      | |  | | |  \| |
| |      |  __|      > <     | |   | |      | |  | | | . ` |
| |____  | |____    / . \   _| |_  | |____  | |__| | | |\  |
|______| |______|  /_/ \_\ |_____|  \_____|  \____/  |_| \_|

````

## Lexicon is custom header file for C++.

Please note that it only works on the Windows.

It is a collection of some simple and some complex functions. These functions can be used to manipulate strings, create animations ( without graphics.h ), etc.

To know more see the [Wiki](https://github.com/Blank-One/HeaderFile-Lexicon/wiki) !!

Also, checkout all the releases [here](https://github.com/Blank-One/HeaderFile-Lexicon/releases) and feel free to report [issues](https://github.com/Blank-One/HeaderFile-Lexicon/issues).

Pull requests and changes are always welcome.
